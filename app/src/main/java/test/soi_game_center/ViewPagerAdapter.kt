package test.soi_game_center

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ViewPagerAdapter internal constructor(fm:FragmentManager):FragmentPagerAdapter(fm){
    private val COUNT = 4

    override fun getItem(position:Int):Fragment?{
        var fragment: Fragment? = null
        when(position){
            0->fragment = Page1()
            1->fragment = page2()
            2->fragment = Page3()
            3->fragment = Achivement_page4()

        }
        return fragment
    }

    override fun getCount(): Int {
      return COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }

}